import requests
import json
## question number 2
class Photo:
    def __init__(self, id, title, url, thumbnailUrl ):
        self.id = id
        self.title = title
        self.url = url
        self.thumbnailUrl = thumbnailUrl

    def __str__(self):
        return f'Photo: \nid: {self.id}, \ntitle: {self.title}, \nurl: {self.url}, \nthumbnailUrl: {self.thumbnailUrl}'

    def __repr__(self):
        return f'Photo({self.id}, {self.title}, {self.url}, {self.thumbnailUrl}'

    def __eq__(self, other):
        if self.id == other.id:
            return True
        return False

    def __gt__(self, other):
        if self.id > other.id:
            return True
        return False

    def __lt__(self, other):
        if self.id < other.id:
            return True
        return False

photo1 = Photo(1, "accusamus beatae ad facilis cum similique qui sunt", "https://via.placeholder.com/600/92c952", "https://via.placeholder.com/150/92c952")
photo2 = Photo(2, "reprehenderit est deserunt velit ipsam", "https://via.placeholder.com/600/771796", "https://via.placeholder.com/150/771796")
photo3 = Photo(3, "officia porro iure quia iusto qui ipsa ut modi", "https://via.placeholder.com/600/24f355", "https://via.placeholder.com/150/24f355")

print(f'photo1 == photo2? {photo1 == photo2}')
print(f'photo1 > photo3? {photo1 > photo3}')
print(f'photo3 < photo1? {photo3 < photo1}')
print(f'photo3 > photo1? {photo3 > photo1}')
print(f'photo1 < photo3? {photo1 < photo3}')
print(photo1)
print(photo2.__repr__())



## qustion number 3
resp = requests.get('http://jsonplaceholder.typicode.com/photos/1')
print(type(resp))
print(resp.status_code)
print(resp.content)
one_photo = json.loads(resp.content)
print(one_photo['id'])
one_photo_from_web = Photo(one_photo['id'], one_photo['title'], one_photo['url'], one_photo['thumbnailUrl'])
print('============== from web:  ')
print(f'photo from web: {one_photo_from_web}')



resp = requests.get('http://jsonplaceholder.typicode.com/photos/2')
one_photo = json.loads(resp.content)
second_photo_from_web = Photo(one_photo['id'], one_photo['title'], one_photo['url'], one_photo['thumbnailUrl'])
print('============== from web:  ')
print(f'photo from web: {second_photo_from_web}')

resp = requests.get('http://jsonplaceholder.typicode.com/photos/2')
one_photo = json.loads(resp.content)
second_second_photo_from_web = Photo(one_photo['id'], one_photo['title'], one_photo['url'], one_photo['thumbnailUrl'])
print('============== from web:  ')
print(f'photo from web: {second_second_photo_from_web}')
print(f'second == second_second? {second_photo_from_web == second_second_photo_from_web}')


## question number 4
class QuickPhoto:
    def __init__(self, data_dict):
        self.__dict__ = data_dict
    def __str__(self):
        result = "QuickPhoto :\n"
        for k, v in self.__dict__.items():
                result += k
                result += " : "
                result += str(v)
                result += '\n'
        return result
    def __eq__(self, other):
        if self.id == other.id:
            return True
        return False


    def __gt__(self, other):
        if self.id > other.id:
            return True
        return False


    def __lt__(self, other):
        if self.id < other.id:
            return True
quick_photo1 = QuickPhoto(one_photo)
print(quick_photo1)
print(QuickPhoto)

## question number 5
list_of_objects = []
for x in range(1, 11):

    resp = requests.get(f'http://jsonplaceholder.typicode.com/photos/{x}')
    make_dict = json.loads(resp.content)
    photo_object = QuickPhoto(make_dict)
    list_of_objects.append(photo_object)

## question number 6


class Album:
    def __init__(self, url, count_of_photos):
        self.list_of_photos = []
        for id in range(1, count_of_photos + 1):
            resp = requests.get(f'{url}/{id}')
            make_dict = json.loads(resp.content)
            photo_object = QuickPhoto(make_dict)
            self.list_of_photos.append(photo_object)
Album_object = Album('http://jsonplaceholder.typicode.com/photos', 10)


## question number 7
id_from_user = int(input("enter your id: "))
resp = requests.get(f'http://jsonplaceholder.typicode.com/photos/{id_from_user}')
make_dict = json.loads(resp.content)
photo_object = QuickPhoto(make_dict)
print(photo_object)

## question number 8
class QuickPhotoRestClient:
    def __init__(self, url):
        self.url = url
    def get(id):
        resp = requests.get(f'{self.url}/2, {id}')
    def post(quick_photo):
        resp = requests.post(f'{self.url}/3, data = lala')
    def put(id, quick_photo):
        resp = requests.put(f'{self.url}, {id}')
    def delete(id):
        resp = requests.delete(f'{self.url}/4, {id}')
photo_json = json.loads((resp.content))
photo_in_web = QuickPhotoRestClient('http://jsonplaceholder.typicode.com/photos')
print(photo_in_web)


