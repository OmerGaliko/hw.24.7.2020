'''
## 1
class Omer:
    def __init__(self, name, age, high):
        self.name = name
        self.age = age
        self.high = high
    def __str__(self):
        all_data = (f'Name: {self.name} \nAge: {self.age} \nHigh: {self.high}')

        return all_data
    def __add__(self, other):
        # ***etgar:
        return (self.name + other)



#### ב'
לא ניתן למחוק שדה מידע

##2 –
__init__

## 3
omer_galiko = Omer(name='omer', age=30, high=1.83)
print(omer_galiko)
print(omer_galiko + ' ' + 'galiko')
##def __init__(self, x = 0, y = 0):

## 4
 #Magic method

## 5
מג'ק מתודות הם פונקציות שלא צריך לקרוא להן באופן ישיר למשל:
__str__  עושים print  רק על המופע והמידע יוצג ללא צורך לקרוא למתודה

## 6
כדאי שהאות הראשונה תתחיל באות גדולה מכיוון שלא נתבלבל בין מחלקות למתודות.

## 7
 __str___ ו - __repr__

## 8
 הערכים שהפונקציות מחזירות הן ערך string עם מידע על האובייקט

## 9
פונקציית add  אומרת לפיתון איך לחבר את האובייקט בפעולת חיבור
פונקציית eq אומרת לפיתון מה להשוות בין האובייקטים הנמצאים בתוך המחלקה והפונקצייה מחזירה True  או  False


## 10

class BankAccount():
    def __init__(self, account_number, first_name, last_name, balance):
        self.account_number = account_number
        self.first_name = first_name
        self.last_name = last_name
        self.balance = balance




    def __str__(self):
        return f'BankAccount: \naccount_number = {self.account_number} \nfirst_name : {self.first_name} \nlast_name = {self.last_name} \nbalance : {self.balance}'

    def __repr__(self):

        return f'BankAccount({self.account_number}, {self.first_name}, {self.last_name}, {self.balance}'
    def __eq__(self, other):
        if self.balance == other.balance:
            return True
        return False
    def __sub__(self, other):
        return self.balance - other.balance
    def __mul__(self, other):
        return self.balance * other.balance
    def __add__(self, other):
        return self.balance + other.balance

    def __gt__(self, other):
        if self.balance > other.balance:
            return True
        return False



omer = BankAccount(365, 'omer', 'g', 2000)
itzik = BankAccount(366, 'itzik', 'm', 10)

print('balances equals?' ,omer == itzik)
print('omer have more balance?', omer > itzik)
print('the sum of omer and itzik', omer + itzik)
print(omer - itzik)
print(omer * itzik)
#print(omer)
#print(itzik)

########### ETGAR



