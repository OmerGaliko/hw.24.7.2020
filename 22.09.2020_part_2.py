'''

### 1
לא כל כך הבנתי את השאלה אז אני פשוט התקדמתי ולא רציתי להיתקע הרבה על שאלה אחת...
### 2
לJSON  יש מבנה נתונים של מילון (DICTIONARY)
הפורמט הוא הוא "key" : "value"

### 3
#d = {'lala': 'omer', 'i': 'u'}

#print(str(d))

### 4
import  json
class Omer:
    def __init__(self, d):
        self.__dict__ = d
    def __str__(self):
        results = ""
        for k, v in self.__dict__.items(): #made dict to string
            results += f'{k} : {v} '
        return results

### 5
המשמעות היא כאשר אתה נכנס לאתר אינטרנט ואתה לוחץ על לחצן כלשהו אז הדפדפן נשאר באותו מקום והוא פותח בcontent אחר.
 מאשר אתרים אחרים שאתה לוחץ על לחצן כלשהו והאתר לא קופץ למקום אחר וכל הדף משתנה

### 6
מה שהמציא את REST WEB SERVICR הוא רוי פילדינג.
•	שירות וובי שבו מבקשים מידע כלשהו ומקבלים תשובה לפי מה שביקשנו
•	הוא מסייע בכך שהוא נותן לנו GET POST PUT ו DELETE

### 7
GET – משגר מידע
POST – הוספה
PUT – לעדכן
DELET – למחוק

### 8
המשפחות הם:
200 – בקשה מול השרת הצליחה
300 – הURL  הוחלף וקיבלנו REDIRECT
400 – בעיה בצד של הלקוח
500 – בעיה בצד של השרת


omer1 = Omer({"omer" : "galiko", "keren" : "scliar"})
d = json.loads('{"omer" : "galiko", "keren" : "scliar"}')
print(d)


### 9
import requests
resp = requests.get('http://jsonplaceholder.typicode.com/photos/1')
print(resp.content)
'''


### 10
##/com.typicode.jsonplaceholder://http  עוזר לנו בכך שיש שם הרבה שמות בדויים שאנחנו יכולים להשתמש. אתר זה נותן לנו לתרגל ולראות מה שאנחנו עושים הוא תקין בכך שאנחנו משתמשים בו ומקבלים RESPONSE 200

### 11
import json
import requests
number = int(input("enter a number: "))
class PhotoFromWeb:
    def __init__(self, d):
        self.__dict__ = d
    def __str__(self):
        results = 'Details of Photo:\n '
        for k, v in self.__dict__.items():
            results += '\n'
            results +=  f'{k} = {v}'
        return  results

resp = requests.get(f'http://jsonplaceholder.typicode.com/photos/{number}')
photo_dict = json.loads(resp.content)
photo1 = PhotoFromWeb(photo_dict)
print(photo1)
