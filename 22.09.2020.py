'''
### 1
דקורטור הוא בעצם שטרודל (כרוכית) ואפשר לעשות אותו כ getter  ו setter
לדוגמא :  כאשר אנחנו רוצים להפעיל את getter  ו setter  במקום לכתוב פעם פונקציה אפשר להשתמש בשטרודל @property = getter @age.setter = setter

### 2
Getter – פונקציה שמביאה את השדה הסודי של המחלקה
Setter-  אחרי שכתבנו שדה מוסתר עם פונקצית getter  ניתן לשנות את השדה המוסתר למספר אחר עם פונקצית setter

### 3
class Mr:
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, value):
        if value >= 0:
            self.__age = value
    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self, value):
        if type(value) == type(""):
            self.__name = value


    def __str__(self):
        return f'Mr {self.name} {self.age}'

omer = Mr('omer', 30)
omer.name = 'Galiko'
print(omer.age)
omer.age = -40
print(omer)

יש להיזהר מ crash  כאשר הגדרנו ערכים לא נכונים במתודות לדוגמא:
Self. __age = None  אם התוכן מוסתר ונתנו לו ערך שהוא מינוס (-50) כאשר רשמנו בתחילת הבנאי None   אז התכנית לא תקרוס ופשוט יהיה רשום None

##4
הפונקציה מדפיסה את המידע של המחלקה

##5
DIR מראה את כל התכונות והפונקציות של המחלקה


##6
class AllMyLife:
    def __init__(self, age):
        self.age = age

class CurrentAge(AllMyLife):

    def my_age_now(self):
        print(f' {self.age}')

omer = CurrentAge(30)
omer.my_age_now()
'''
### 7
עושה סדר לחיפוש אחר המתודה

### 8
פולימורפיזם כאשר קוראים לפונקציה אז הוא מחפש, משנה, מוצא את הפונקציה המתאימה לפעולה מתאימה בהתאם למופע של אותה המחלקה שעושה את הפעולה

### 9
Override  - זה בעצם דריסת פונקציה שנמצאת במחלקה גבוהה מאיתנו בעץ ירושה  (הבן דורס את הפונקציה של האבא)

### 10
הפונקציה SUPER מפנה אותנו למחלקה הראשונה שייצרנו (מחלקה של האבא)

### 11
כדאי לקרוא מהבנאי של הבן לבנאי של האבא מכיוון שאם יש לנו הרבה שדות מחלקה בבנאי של האבא אז נוכל לקצר בכך שנקרא לבנאי של הבא דרך הבנאי של הבן והקוד יהיה יותר קצר ומסודר

### 12
מכיוון שאם נרצה את הפרמטרים במחלקה של האבא ונרצה להדפיס\להוסיף אז נצטרך לקרוא לSTR של האבא במחלקה של הבן

### 13
דרך לצייר עצי ירושה, נשתמש בו כאשר נרצה לדעת מה נמצא בכל מחלקה באופן ברור ובאיזה שדות מידע נוכל להשתמש (שדות מיוחדים למחלקה ספציפית, האם יש GETTER או SETTER באחת מהמחלקות וכולי...)



### 14
class Shape:
    def __init__(self, shape):
        self.shape = shape
    def __str__(self):
        return f'{self.shape}'


class FourSides(Shape):
    def __init__(self, a, b, c, d):
        super().__init__('square')
        self.a = a
        self.b = b
        self.c = c
        self.d = d

    def getHekef(self):
        perimeter = self.a + self.b + self.c + self.d
        return perimeter

    def __str__(self):
        return super().__str__() + f' {self.a},  {self.b},  {self.c},  {self.d}, {self.getHekef()}'


omer = FourSides(30, 5, 5, 5)
omer.getHekef()
print(omer)








